/*
 * demonstratie voor een controller van de lift.
 * 
 * De demo maakt verbinding met de liftkooi simulator en de lift simulator.
 */
package lift.demo;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* geimporteerde packages */
import lift.lib.Factory;
import lift.lib.inf.iLift;
import lift.lib.inf.iLiftKooi;

/**
 *
 * @author JSC
 * @version 0.1
 * 
 */

/**
 * 
 * @author bedirhan
 * 0x3f = 0
 * 0x06 = 1
 * 0x5B = 2
 * 0x79 = E
 */

public class Controller
{
    static iLiftKooi kooi;
    
    /* Maak een object aan dat communiceert met de lift simulator */
    static iLift lift;
    
    /**
     * static void main() is de methode waar alle java programma's mee
     * opstarten.
     * 
     * De main() in deze demo wordt gebruikt om enkele lift aanroepen
     * te demonstreren.
     * 
     * @param args - mogelijkheid om argumenten aan het programma door
     * te geven vanuit de omgeving. Dit is niet gebruikt.
     * 
     */
    public static void main( String[] args )
    {
        /* Maak een object aan dat de communicatie verzorgt met de lift kooi.
         * In dit geval is er gekozen voor de liftkooi simulator.
         */
        kooi = Factory.getLiftKooi("sim");
        
        /* Maak een object aan dat communiceert met de lift simulator */
        lift = Factory.getLift();
        
        /*
         * Oneindige lus
         */
        while ( true )
        {
            /*
             * Lees de waarden van de knoppen uit van de lift kooi
             */
        	
            int cagePosition = lift.getCagePosition(); // 2	
            int switches = lift.getSwitches();
            int speed = 0x05;
            
            System.out.println(cagePosition);
            
            /* Op basis van de waarden van de knoppen kunnen de deuren worden
            * geopend. 
            * Uiteraard is dit in het echt niet de bedoeling.
            * 
            */
            
            //System.out.println("Knoppen: " + switches);

            setCageLeds(cagePosition);
            getCagePressedButtons(switches);
            
            //lift.setMotorSpeed(0x05);
    		//lift.setMotorDirection(true);
    		
    		//System.out.println(cagePosition);
            
            //startMotor(speed, cagePosition, 2);
       
            if ( switches == 0x01 )
            {
            	System.out.println("Knop 1 ingedrukt");
            	
            	//openAndCloseDoors(cagePosition, speed, switches);
            	startMotor(speed, cagePosition, 2);
            }
            else if ( switches == 0x02 )
            {
            	System.out.println("Knop 2 ingedrukt");
            	
            	//openAndCloseDoors(cagePosition, speed, switches);
            	startMotor(speed, cagePosition, switches);
            }
            else if ( switches == 0x04 ) {
            	
            	System.out.println("Knop 3 ingedrukt");
            	
            	//openAndCloseDoors(cagePosition, speed, switches);
            	startMotor(speed, cagePosition, switches);
            }
            else
            {
                // Doe niets
            }
            
            /*
            * Optioneel (maar wel handig om te doen): 
            * 
            * Even wachten om te voorkomen dat de PC overbelast wordt.
            * Er wordt 100 ms gewacht. Dat betekent dat deze lus 10x
            * per seconde wordt uitgevoerd. 
            * 
            * Dit hele proces stopt daadwerkelijk 100ms.
            */
            sleep( 100 );
        }   
    }
    
    /**
     * De methode zorgt dat het huidige proces stopt gedurende de tijd
     * die wordt opgegeven
     * 
     * @param msec Aantal millisec  
     */
    private static void sleep( int msec )
    {
        try
        {
            java.lang.Thread.sleep( msec );
        }
        catch (InterruptedException ex)
        {
            // Doe niets
        }
    }
    
    public static void openDoors(int cagePosition, int motorSpeed, int switches) {
    	
    	char sensor = (char)encoder(cagePosition).values().toArray()[0];
    	
    	if(motorSpeed == 0) {
    		if(sensor == '-') {
    			if(lift.getDoorSignals() != 0) {
	    			if(lift.getDoorSignals() == 1) {
	    				lift.setDoorOpen();
	    			}
    			}
    		}
    	}
    }
    
    public static void closeDoors(int cagePosition, int motorSpeed, int switches) {
    	
    	char sensor = (char)encoder(cagePosition).values().toArray()[0];
    	
    	if(motorSpeed == 0) {
    		if(sensor == '-') {
    			if(lift.getDoorSignals() != 0) {
	    			if(lift.getDoorSignals() == 2) {
	    				lift.setDoorClose();
	    			}
    			}
    		}
    	}
    }
    
    /*public static void openAndCloseDoors(int cagePosition, int motorSpeed, int switches) {
    	
    	char sensor = (char)encoder(cagePosition).values().toArray()[0];
    	
    	if(motorSpeed == 0 && cagePosition == switches) {
    		if(sensor == '-') {
    			if(lift.getDoorSignals() != 0) {
	    			if(lift.getDoorSignals() == 1) {
	    				lift.setDoorOpen();
	    			} else {
	    				lift.setDoorClose();
	    			}
    			}
    		}
    	}
    }*/
    
    public static int getFloor(int decimal) {
    	
    	int floor = 0;
    	
    	switch(decimal) {
			case 3:
				floor = 0;
			case 7:
				floor = 1;
			case 11:
				floor = 2;
		}
    	
    	return floor;
    }
    
    public static void startMotor(int motorSpeed, int cagePosition, int switches) {
 
    	int liftvraag = switches;
    	int decimal = (int)encoder(cagePosition).keySet().toArray()[0];
    	int verdieping = getFloor(decimal);
    	
    	if(verdieping == liftvraag) {
    		motorSpeed = 0x00;
    		lift.setMotorSpeed(motorSpeed);
    	} else if(verdieping < liftvraag) {
    		if(motorSpeed != 0x00) {
    			if(lift.getDoorSignals() == 1) {
    				lift.setMotorSpeed(motorSpeed);
		    		lift.setMotorDirection(true);	
    	    	}
    		}
    	} else if(verdieping > liftvraag) {
    		if(motorSpeed != 0x00) {
    			if(lift.getDoorSignals() == 1) {
    				lift.setMotorSpeed(motorSpeed);
		    		lift.setMotorDirection(false);	
    	    	}
    		}
    	}
    	
    	System.out.println("Cage Position: " + cagePosition + " Liftvraag: " + liftvraag);
    	
    	/*if(decimal != switches) {
	    	if(motorSpeed != 0x00) {
				if(lift.getDoorSignals() == 1) {
					
					if(cagePosition > switches) {
						lift.setMotorSpeed(motorSpeed);
			    		lift.setMotorDirection(false);
					} else {
						lift.setMotorSpeed(motorSpeed);
			    		lift.setMotorDirection(true);
					}
					
		    	}
	    	}
    	} else {
    		motorSpeed = 0x00;
    		lift.setMotorSpeed(motorSpeed);
    		openDoors(cagePosition, motorSpeed, switches);
    	}*/

    }
    
    public static int getCagePressedButtons(int switchValue) {
    	
    	int pressedButton = 0;
    	
    	if(switchValue > 0) {
    		pressedButton = switchValue;
    		//System.out.println(pressedButton);
    	}
    	
    	return pressedButton;
    }
    
    /**
     * 
     * @param cagePosition
     * This method updates the led.
     * The input is the Cage Position of the type integer.
     * The input is used to get information from the encoder.
     * With the information we set the display of the leds.
     * 
     * Exception:
     * 
     * If the input is incorrect the system will exit and show an exception.
     */
    public static void setCageLeds(int cagePosition) {
    	
    	int decimal = (int)encoder(cagePosition).keySet().toArray()[0];
    	char sensor = (char)encoder(cagePosition).values().toArray()[0];
    	
    	char led = '0';
    	
    	if(decimal == 0 && sensor != '-') {
    		kooi.setLeds(0x79);
   	   		led = 'E';
    	}
    	
    	if(decimal < 4) {
    		led = '0';
   		  	kooi.setLeds(0x3f);
   	   	}
    	else if(decimal > 3 && decimal < 8) {
   	   		led = '1';
   	   		kooi.setLeds(0x06);
   	   	} else if(decimal > 7 && decimal < 11) {
   	   		led = '2';
			kooi.setLeds(0x5B);
   	   	} else {
   	   		kooi.setLeds(0x79);
   	   		led = 'E';
   	   	}
    	
    	/*System.out.println("Cage position: " + cagePosition);
    	System.out.println("Decimal: " + decimal);
    	System.out.println("Sensor: " + sensor);
    	System.out.println("Led: " + led + "\n");*/
    	
    }
    
    /**
     * @param decimal
     * 
     * This method encodes grey code to a linear numeral system.
     * A HashMap keeps track of the cage position and the state of the cage.
     * There are three states: Under, middle, or above.
     * This information gets compressed in a HashMap and returned for usage.
     * 
     * Exception:
     * 
     * If the input is incorrect the system will exit and show an exception.
     * @return HashMap
     */
    public static HashMap<Integer, Character> encoder(int decimal) {
    	
    	HashMap<Integer, Character> mapping = new HashMap<Integer, Character>();

    	/**
    	 * O = Onder, M = Midden, B = Boven
    	 */
    	
	    	switch(decimal) {
	    		// Danger elevator, because it under the ground floor.
	    		case 0: 
	    			decimal = 0;
	    			mapping.put(decimal, 'O');
	    		break;
	    		case 1:
	    			decimal = 1;
	    			mapping.put(decimal, 'M');
	    		case 3:
	    			decimal = 2;
	    			mapping.put(decimal, 'B');
	    			break;
	    		case 2:
	    			decimal = 3;
	    			mapping.put(decimal, '-');
	    			break;
	    		case 6:
	    			decimal = 4;
	    			mapping.put(decimal, 'O');
	    			break;
	    		case 7:
	    			decimal = 5;
	    			mapping.put(decimal, 'M');
	    			break;
	    		case 5:
	    			decimal = 6;
	    			mapping.put(decimal, 'B');
	    			break;
	    		case 4:
	    			decimal = 7;
	    			mapping.put(decimal, '-');
	    			break;
	    		case 12:
	    			decimal = 8;
	    			mapping.put(decimal, 'O');
	    			break;
	    		case 13:
	    			decimal = 9;
	    			mapping.put(decimal, 'M');
	    			break;
				case 15:
					decimal = 10;
					mapping.put(decimal, 'B');
				    break;
				case 14:
					decimal = 11;
					mapping.put(decimal, '-');
					break;
				default:
					mapping.put(decimal, 'E');
					break;
				/*default:
					try {
			    		throw new Exception();
					} catch (Exception e) {
						System.out.println("Input invalid.");
						System.exit(1);
					}*/
	    	}
    	
    	return mapping;
   
    }
}
