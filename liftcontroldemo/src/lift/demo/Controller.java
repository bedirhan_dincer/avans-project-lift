/*
 * demonstratie voor een controller van de lift.
 * 
 * De demo maakt verbinding met de liftkooi simulator en de lift simulator.
 */
package lift.demo;

/* geimporteerde packages */
import lift.lib.Factory;
import lift.lib.inf.iLift;
import lift.lib.inf.iLiftKooi;

/**
 *
 * @author JSC
 * @version 0.1
 * 
 */
public class Controller
{
    static iLiftKooi kooi;
    
    /* Maak een object aan dat communiceert met de lift simulator */
    static iLift lift;

    /**
     * static void main() is de methode waar alle java programma's mee
     * opstarten.
     * 
     * De main() in deze demo wordt gebruikt om enkele lift aanroepen
     * te demonstreren.
     * 
     * @param args - mogelijkheid om argumenten aan het programma door
     * te geven vanuit de omgeving. Dit is niet gebruikt.
     * 
     */
    public static void main( String[] args )
    {
        /* Maak een object aan dat de communicatie verzorgt met de lift kooi.
         * In dit geval is er gekozen voor de liftkooi simulator.
         */
        kooi = Factory.getLiftKooi("sim");
        
        /* Maak een object aan dat communiceert met de lift simulator */
        lift = Factory.getLift();
        
        
        /*
         * Oneindige lus
         */
        while ( true )
        {
            /*
             * Lees de waarden van de knoppen uit van de lift kooi
             */
            int switches = kooi.getSwitchValues();


            /* Op basis van de waarden van de knoppen kunnen de deuren worden
            * geopend. 
            * Uiteraard is dit in het echt niet de bedoeling.
            */
            if ( switches == 0x01 )
            {
                lift.setDoorOpen();
            }
            else if ( switches == 0x02 )
            {
                lift.setDoorClose();
            }
            else
            {
                // Doe niets
            }
            
            /*
            * Optioneel (maar wel handig om te doen): 
            * 
            * Even wachten om te voorkomen dat de PC overbelast wordt.
            * Er wordt 100 ms gewacht. Dat betekent dat deze lus 10x
            * per seconde wordt uitgevoerd. 
            * 
            * Dit hele proces stopt daadwerkelijk 100ms.
            */
            sleep( 100 );
        }   
    }
    
    
  
 
   
    
    /**
     * De methode zorgt dat het huidige proces stopt gedurende de tijd
     * die wordt opgegeven
     * 
     * @param msec Aantal millisec  
     */
    private static void sleep( int msec )
    {
        try
        {
            java.lang.Thread.sleep( msec );
        }
        catch (InterruptedException ex)
        {
            // Doe niets
        }
    }
}
